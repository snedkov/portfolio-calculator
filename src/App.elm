module App exposing (..)

import Browser exposing (..)
import Html exposing (..)
import Html.Attributes exposing (..)
import Html.Events exposing (onInput, onClick)

-- MAIN

main =
  Browser.sandbox { init = init, view = view, update = update }

-- MODEL

type alias Model =
  { value: Float
  , positions: List Position
  , newPosition: Position
  }

type alias Position =
  {
    id: Int
  , ticker: String
  , percent: Float
  , cost: Float
  , quantity: Float
  }

init : Model
init =
  Model 0 [] (Position 1 "" 0 0 0)

-- UPDATE

type Msg
 = Value String
 | AddPosition
 | ChangeNewPositionTicker String
 | Percent Int String
 | Cost Int String

update : Msg -> Model -> Model
update msg model =
  case msg of
    Value value ->
      case String.toFloat value of
        Just floatValue ->
          { model | value = floatValue }
          |> recalculateQuantities
        
        Nothing ->
          { model | value = 0 }

    AddPosition ->
      { model | positions = model.positions ++ [ Position model.newPosition.id model.newPosition.ticker 0 0 0 ] }

    ChangeNewPositionTicker ticker ->
      { model | newPosition = Position (model.newPosition.id + 1) ticker 0 0 0 } -- We increment the id a bit often, but that's alright for now

    Percent id percent ->
      case String.toFloat percent of
        Just floatValue ->
          updatePercent model id floatValue
          |> recalculateQuantities

        Nothing ->
          model

    Cost id cost ->
      case String.toFloat cost of
        Just floatValue ->
          updateCost model id floatValue
          |> recalculateQuantities

        Nothing ->
          model

updatePercent : Model -> Int -> Float -> Model
updatePercent model id percent =
  Model model.value (List.map (\position ->
      if position.id == id then
        { position | percent = percent }
      else
        position
    ) model.positions) model.newPosition

-- Duplicate with above, only apply to different property
updateCost : Model -> Int -> Float -> Model
updateCost model id cost =
  Model model.value (List.map (\position ->
      if position.id == id then
        { position | cost = cost }
      else
        position
    ) model.positions) model.newPosition

-- quantity = (value * percent) / (cost * 100)
recalculateQuantities : Model -> Model
recalculateQuantities model =
  Model model.value (List.map (\position -> { position | quantity = (model.value * position.percent) / (position.cost * 100) } ) model.positions) model.newPosition


-- VIEW

view : Model -> Html Msg
view model =
  div []
  [
    div []
    [
      input [ type_ "text", placeholder "New ticker", value model.newPosition.ticker, onInput ChangeNewPositionTicker ] []
    , text " "
    , button [ onClick AddPosition ] [ text "Add" ]
    , text " "
    ]
  , div []
    [
      input [ type_ "number", placeholder "Value", value (String.fromFloat model.value), onInput Value ] []
    ]
  , hr [] []
  , div []
    [
      input [ readonly True, placeholder "Ticker"] []
    , text " "
    , input [ readonly True, placeholder "Percent"] []
    , text " "
    , input [ readonly True, placeholder "Cost"] []
    , text " "
    , input [ readonly True, placeholder "Quantity"] []
    ]
  , div [] (List.map viewPosition model.positions)
  ]

viewPosition: Position -> Html Msg
viewPosition position =
  div []
  [
    hr [] []
  , input [ readonly True, value position.ticker] []
  , text " "
  , input [ type_ "number", value (String.fromFloat position.percent), onInput (\str -> Percent position.id str)] []
  , text " "
  , input [ type_ "number", value (String.fromFloat position.cost), onInput (\str -> Cost position.id str)] []
  , text " "
  , input [ readonly True, type_ "number", value (String.fromFloat position.quantity)] []
  ]
